<div>
  <img src='./assets/fotoRafael.jpg' align="left" width="100" alt="img"
 <h1>Olá! Eu sou o Rafael Cavalcante</h1>
  <ul style="list-style-type: none; margin-left: 20px;">
    <li>Possuo fortes conhecimentos em Front end usando JavaScript, HTML5, Sass e Less, React, Vue e Angular</li>
    <li>No Back end eu domino Node.js e criação de Apis REST usando Express.js, e possuo uma base com Next.js, PHP, Java, C</li>
    <li>De Banco de Dados tenho experiência com MariaDB, MySQL, PostgreSQL, MongoDB e Firebase cloud</li>
    <li>Possuo conhecimentos em Scrum e estrutura de dados</li>
    <li>E estou estudando: Wordpress, Woocommerce, Firebase, React-native, Kotlin, Docker</li>
  </ul>
</div>
 <div>
  <a href="https://github.com/RafaelCava">
</div>
<details>

## Tecnologias
 
 <div style="display: inline_block"><br>
  <img align="center" alt="mongoDB" width="60" src="https://img.icons8.com/color/452/mongodb.png" />
  <img align="center" alt="mysql" width="60" src="https://planet.mysql.com/images/planet-logo.svg" />
  <img align="center" alt="Rafa-HTML" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
  <img align="center" alt="Rafa-CSS" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
  <img align="center" width="60" alt="Sass" src="https://rawgit.com/sass/sass-site/master/source/assets/img/logos/logo.svg" /> 
  <img align="center" alt="git" width="60" src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg"/>
  <img align="center" alt="GitHub" width="60" src="https://github.com/Aakarsh-B/trying-repos/blob/master/github.svg" />
  <img align="center" alt="Rafa-React" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original.svg">
 </div>
 
## Linguagens de Programação
 
<div style="display: inline_block"><br>
  <img align="center" alt="Rafa-Js" width="60" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
  <img align="center" alt="php" width="80" src="https://www.php.net//images/logos/new-php-logo.svg" />
  <img align="center" alt="typerscript" width="60" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Typescript_logo_2020.svg/600px-Typescript_logo_2020.svg.png" />
</div>
 
## Softwares
 <div style="display: inline-block"><br>
  <img align="center" alt="Visual Studio Code" width="60" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png" />
  <img align="center" alt="xampp" width="60" src="https://cdn.worldvectorlogo.com/logos/xampp.svg" />
  <img align="center" alt="Node.js" width="60" src="https://icon-library.com/images/node-js-icon/node-js-icon-11.jpg" />
  <img align="center" alt="firebase" width="60" src="https://cdn.icon-icons.com/icons2/691/PNG/512/google_firebase_icon-icons.com_61474.png" />
  <img align="center" alt="wordpress" width="60" src="https://d29fhpw069ctt2.cloudfront.net/icon/image/38759/preview.svg" />
  <img align="center" alt="elementor" width="60" src="https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/109_Elementor_logo_logos-512.png" />
  <img align="center" alt="woocommerce" width="60" src="https://www.logolynx.com/images/logolynx/f7/f785bb835ec5f430b84f6f552b8bf1b6.png" />
 </div><br>
 <br>
 
## Redes sociais 
<div style="display: inline-block"><br> 
  <a href = "mailto: jogosmaneiros.rafael@gmail.com" target="_blank"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
  <a href="https://www.linkedin.com/in/rafael-cavalcante-148a54143/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>
 
## Formação Acadêmica
 <img align="center" width="80" src="https://cubos.io/marca-cubosacademy.4a9e1907.svg" alt="cubos academy" />
 <img align="center" width="120" src="https://cursos.dankicode.com/app/Views/public/images/danki_logo.png" />

 [![Wakatime](https://wakatime.com/share/@bbe123b1-e55a-4764-b05b-ef53394499b1/3091a5e6-ca43-4960-9a81-0ff47bdaccf5.png)](https://wakatime.com/)

</details>

#
 
 <div align="center">
  
<div>
  
 [![Anurag's GitHub stats](https://github-readme-stats.vercel.app/api?username=RafaelCava&show_icons=true&theme=synthwave)](https://github.com/anuraghazra/github-readme-stats)
  [![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=RafaelCava)](https://github.com/anuraghazra/github-readme-stats)
  
   </div>
   
#
   
  [![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=RafaelCava&repo=landing_page&show_owner=true)](https://github.com/RafaelCava/landing_page)
 [![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=RafaelCava&repo=api-crud-mongo&show_owner=true)](https://github.com/RafaelCava/api-crud-mongo)
 [![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=RafaelCava&repo=Hostinger-portfolio&show_owner=true)](https://github.com/RafaelCava/Hostinger-portfolio)
  [![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=RafaelCava&repo=Instagram_clone&show_owner=true)](https://github.com/RafaelCava/Instagram_clone)
 
 </div>
 
 <div align="center">
  
# Portfólio
 
 [Portfolio](http://portfolio-rafael.site)
   
 [Api-Express-Typescript](https://api-types-rafael.herokuapp.com/docs/)
   
 [Portfolio Vercel](http://impacttecnologia.online/)
   
 [TodoApp](https://todolist-f3156.web.app/)
   
 [Instagram Clone](https://instagram-clone-17c8f.web.app/)
   
 </div>
